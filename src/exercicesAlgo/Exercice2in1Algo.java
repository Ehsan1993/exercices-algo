package exercicesAlgo;
import java.util.Scanner;

public class Exercice2in1Algo {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		System.out.println("x = 10\ny=20\n\nSi Z est:\n20 > Z > 10\n");
		int Z;
	
		do {
			System.out.print("Z = ");
			Z = scanner.nextInt();
			if (Z<=10) {
				System.out.println("Plus grand");
			}
			else if (Z>20) {
				System.out.println("Plus petit");
			}
			else{
				System.out.println("La reponse est bonne!!!");
			}
		}
		while(Z>20 || Z<10);{}
		scanner.close();
	}
}




