package exercicesAlgo;
import java.util.Random;
import java.util.Scanner;
//import java.util.Arrays;

public class Exercice3in4Algo {

	public static Scanner scanner = new Scanner (System.in);
	public static void main(String[] args) {

		int i;		//index for the table/row
		int j;		//index for the table /column
		int Max = 0;// le chiffre le plus grand dans le tableau;
		
		Random random = new Random();
		
		System.out.println("Veuillez saisir le nombre de tableau?");
		Integer [][] tab = new Integer [scanner.nextInt()][scanner.nextInt()];
		
		
		for (i = 0; i<tab.length; i++) {
			for (j = 0; j<tab[i].length; j++) {
				tab[i][j] = random.nextInt(30);
			}
		}
		for (i = 0; i<tab.length; i++) {
			for (j = 0; j<tab[i].length; j++) {
				System.out.print(tab[i][j] + "\t");
			}
			System.out.println();
		}
		for (i = 0; i<tab.length; i++) {
			for(j=0; j<tab[i].length; j++) {
				if (tab[i][j]>Max) {
					Max = tab[i][j];}
				}
			}
		System.out.print("\nle chiffre le plus grand est: ");
		System.out.println(Max);
		}
}