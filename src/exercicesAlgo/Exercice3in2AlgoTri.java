package exercicesAlgo;
import java.util.Scanner;
public class Exercice3in2AlgoTri {

	public static Scanner scanner = new Scanner (System.in);
	public static void main(String[] args) {

		// tri à insertion d'un tableau tab 
		
		
		int i;
		int j;
		int remplacant;	// une place pour mettre les chiffres pour faire le tri;
		
		int [] tab = {12, 32, 433, 54, 65, 1, 76, 43, 32};
		
			for (i=0; i<tab.length; i++) {
				for (j = i+1; j<tab.length; j++) {
					remplacant = 0;
					if(tab[i]>tab[j]){
						remplacant = tab[i];
						tab[i] = tab [j];
						tab[j] = remplacant;
					}
				}
				System.out.print(tab[i] + "\t");	
			}	
	}
}