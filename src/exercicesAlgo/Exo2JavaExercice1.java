package exercicesAlgo;

import java.util.Arrays;
import java.util.Random;

import javax.swing.JOptionPane;

public class Exo2JavaExercice1 {

	public static void main(String[] args) {
		String tailleDeTableauString = JOptionPane.showInputDialog(null, "Veuillez saisir la taille de tableau", "Tableau", JOptionPane.QUESTION_MESSAGE);
		int tailleDeTableauEntier = Integer.parseInt(tailleDeTableauString);
		
		Random random = new Random();
		
		//le chiffre le plus grand dans le tableau
		int Max = 0;	
		
		int [] tab = new int [tailleDeTableauEntier];
			for (int i=0; i<tab.length; i++) {
				tab [i] = random.nextInt(70);
				if (tab [i]>Max) {
					Max = tab[i];
				}
			}
			System.out.println(Arrays.toString(tab));
			JOptionPane.showMessageDialog(null, "le chiffre le plus grand est : " + Max, "Max", JOptionPane.QUESTION_MESSAGE);

	}
}
