package exercicesAlgo;

import javax.swing.JOptionPane;

public class Exercice4Puissance {
		
	public static int main(Integer[] args) {
		
		String valeurDeX = JOptionPane.showInputDialog("Veuillez saisir la valeur de X");
		int x = Integer.parseInt(valeurDeX); // Valeur de X en entier
		
		String valeurDeY = JOptionPane.showInputDialog("Veuillez saisir la valeur de Y");
		int y = Integer.parseInt(valeurDeY); // Valeur de Y en entier
		
		int Resultat = 1;
		
		for (int i=0; i<y; i++) {
			Resultat = x*Resultat;
		}
		JOptionPane.showMessageDialog(null, "La valeur de " + x + " à la puissance de " + y + " est : " + Resultat);

		return Resultat;
	}
	
}
