package exercicesAlgo;

import javax.swing.JOptionPane;

public class SapinDeNoel {

	public static void main(String[] args) {

		
		String numberOfLines = JOptionPane.showInputDialog("Pour voir \"Sapin de Noel\", veuillez saisir la taille d'arbre");
		int numberOfLinesEntier = Integer.parseInt(numberOfLines);

			for (int i = 1; i<numberOfLinesEntier; i++) {
				for (int j = 1; j<=numberOfLinesEntier; j++) {
					if ((i+j) >= numberOfLinesEntier+1) {
						System.out.print("*");
					}
					System.out.print(" ");
				}
				System.out.println("");
			}
	}
}