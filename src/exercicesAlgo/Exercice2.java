package exercicesAlgo;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Exercice2 {

	public static Scanner scanner = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String N;
		int M;
		int i;
		N = JOptionPane.showInputDialog("Veuillez saisir un entier");
		M = Integer.parseInt(N);
		System.out.print("M = ");
		System.out.println(M);
		
		JOptionPane.showMessageDialog(null, "Pour trouver la factorielle de " + N + ", veuillez regarder sur l'écran");
		M = Integer.parseInt(N);
		for (i = M; i > 2; i--) {
			M = (i-1)*M;
			System.out.println(M);
		}
		JOptionPane.showMessageDialog(null, "La factorielle de " + N + ", est " + M);

	}
}
