package exercicesAlgo;

import javax.swing.JOptionPane;

public class Exercice6FactorielleTableau {

	public static void main(String[] args) {

		Integer [][] tab = new Integer [2][10];
		
		String numeroSaisiString;
		int i;
		int j;
		int numeroSaisiEntier;
		
		//Boucle pour remplir le tableau
		for (i=0; i<tab[0].length; i++) {
			tab[0][i] = i + 1;
			tab[1][i] = Factorielle.factorielle(i+1);
		}
			
		//Boucle pour afficher le tableau
		for (j = 0; j<tab.length; j++) {
			for (i = 0; i<tab[j].length; i++){
				System.out.print(tab [j][i] + "\t" );
			}
		System.out.println(" ");

		}
		
		try {
			numeroSaisiString = JOptionPane.showInputDialog("Pour quelle valeur souhaitez-vous calculer la factorielle (de 1 à 10 max");
			numeroSaisiEntier = Integer.parseInt(numeroSaisiString);
			
			if (numeroSaisiEntier <= 10 && numeroSaisiEntier >= 1){
				JOptionPane.showMessageDialog(null, "La factorielle de " + numeroSaisiString + " est: " + tab [1][numeroSaisiEntier - 1]);
			}
			else {
				JOptionPane.showMessageDialog(null, numeroSaisiEntier + "\nVeuillez saisir un chiffre entre 1 et 10 ");
			}
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, "Invalid, please saisir un chiffre entre 1 et 10.\n " + e.getMessage());
		}
	}
}

