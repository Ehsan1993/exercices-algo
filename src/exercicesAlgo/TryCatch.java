package exercicesAlgo;

import java.util.Scanner;

public class TryCatch {
	public static Scanner scanner = new Scanner(System.in);
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		System.out.println("x = 10");
		System.out.println("y = 20");
		System.out.println("z = x + y ");

		int z=0;
		
		
		do{
			
			System.out.print("z = ");
			try {
			z = scanner.nextInt();
			if(z!=30) {
				System.out.println("Sorry,\nYou entered the wrong number.");
				}
			}
			catch(Exception e){
				scanner.nextLine();
				System.out.println("Invalid Number.\nPlease try again.");
			}					

		}
		while(z != 30);{}
		
		System.out.println("The answer is right");

	}
}

