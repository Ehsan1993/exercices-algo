package exercicesAlgo;
import java.util.Scanner;
public class Exercice3in1Algo {

	public static Scanner scanner = new Scanner (System.in);
	public static void main(String[] args) {

		int i;			//index for the table
		int total = 0;	//total of the table
		
		
		System.out.println("Veuillez saisir le nombre de tableau?");
		Integer [] tab = new Integer [scanner.nextInt()];
		
		for (i = 0; i<tab.length; i++) {
			System.out.println("Veuillez saisir un chiffre");
			tab [i] = scanner.nextInt();
			total = total + tab [i];
		}
		
		
		System.out.print("Le moyen des chiffres est: " + (double)total/tab.length);

	}

}
