package exercicesAlgo;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String choisirDExercice = JOptionPane.showInputDialog("Quelle exercice voulez-vous ouvrir?\n\n"
				+ "\n1. Exercice 1"
				+ "\n2. Exercice 2"
				+ "\n3. Exercice 2.1 Algo"
				+ "\n4. Exercice 2.2 Algo"
				+ "\n5. Exercice 3.1 Alog"
				+ "\n6. Exercice 3.2 AlgoTri"
				+ "\n7. Exercice 3.4 Algo"
				+ "\n8. Exercice 4 Puissance"
				+ "\n9. Exercice 6 Factorielle dans le tab"
				+ "\n10. Exercice TryCatch"
				+ "\n\n"
				+ "");
		int numberOfExercice = Integer.parseInt(choisirDExercice);
		
		
		
		switch (numberOfExercice) {
		case 1:
			Exercice1.main(null);
			break;
		case 2:
			Exercice2.main(null);
			break;
		case 3:
			Exercice2in1Algo.main(null);
			break;
		case 4:
			Exercice2in2AlgoFactorielle.main(null);
			break;
		case 5:
			Exercice3in1Algo.main(null);
			break;
		case 6:
			Exercice3in2AlgoTri.main(null);
			break;
		case 7:
			Exercice3in4Algo.main(null);
			break;
		case 8:
			Exercice4Puissance.main(null);
			break;
		case 9:
			Exercice6FactorielleTableau.main(null);
			break;
		case 10:
			TryCatch.main(null);
			break;
			
		}
		
	}
}
